package com.example.smartbar;

import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.widget.Toast;

public class AlarmReciever extends BroadcastReceiver
{
	//Constants for launching APPs
	public static final String UPDATE1 = "UPDATE1";
	public static final String UPDATE2 = "UPDATE2";
	public static final String UPDATE3 = "UPDATE3";
	public static final String UPDATE4 = "UPDATE4";

	@Override
    public void onReceive(Context context, Intent intent)
    {		
	    //Get Previous app from settings
	    SharedPreferences set = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
		PackageManager pm = context.getPackageManager();
		
		if (intent.getAction() != null) {		
			//If the acction contains UPDATE launch the corresponding app
			if(intent.getAction().contains("UPDATE")) {		
				//If application exists
				if(isPackageExisted(context, set.getString(intent.getAction().toLowerCase(), ""))){
					Intent LaunchIntent = pm.getLaunchIntentForPackage(set.getString(intent.getAction().toLowerCase(), ""));
			        context.startActivity(LaunchIntent);
				} else {
					//Check if variables are passed in.
					if(set.getString(intent.getAction().toLowerCase(), "") == ""){
						Toast.makeText(context, "No Package name passed in. Cannot launch any application.", Toast.LENGTH_SHORT).show();
					} else {
						Toast.makeText(context, "This application isnt installed and cannot be started", Toast.LENGTH_SHORT).show();
						//ToDo - Remove APP from database if it doesnt exist
					}
				}
			}
			
			//Launch services
		 	context.startService(new Intent(context, controler.class));
		 	context.startService(new Intent(context, manager.class));	
	 	}
	 	
		//Launch services based on Alarm Manager
	 	if(intent.getIntExtra("run", -1) == 1){
	 		context.startService(new Intent(context, controler.class));
	 	} else if(intent.getIntExtra("run", -1) == 2){
	 		context.startService(new Intent(context, manager.class));
	 	} else if(intent.getIntExtra("run", -1) == 3){
	 		context.startService(new Intent(context, dbctrl.class));
	 	}
     }
	
	//Check if Package is installed
	public boolean isPackageExisted(Context context, String targetPackage){
	    List<ApplicationInfo> packages;
	    PackageManager pm = context.getPackageManager(); 
	    packages = pm.getInstalledApplications(0);
	    
	    for (ApplicationInfo packageInfo : packages) {
		    if(packageInfo.packageName.equals(targetPackage)) return true;
		}        
	    return false;
	}
      
}