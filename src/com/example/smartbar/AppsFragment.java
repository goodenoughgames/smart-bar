package com.example.smartbar;
 
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.example.smartbar.adapter.AppsDrawerListAdapter;

import android.app.ListFragment;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
 
public class AppsFragment extends ListFragment {
     
    public AppsFragment(){}
    
	//SQLiteHelper variables
	private SQLiteHelper dbHelper;
	private SQLiteDatabase db;
    
	private PackageManager packageManager = null;
	private List<ApplicationInfo> applist = null;
	private List<String> disabled_apps = null;
	private AppsDrawerListAdapter listadaptor = null;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_apps, container, false); 
        
		packageManager = getActivity().getPackageManager();
		new LoadApplications().execute(); 
        return rootView;
    }
    
    private List<String> load_disabled_apps(){
    	List<String> disabled_apps = new ArrayList<String>();
    	
    	//Initialize SQLiteHelper
		dbHelper = new SQLiteHelper(getActivity());
	    open();
		Cursor mCursor = db.query(SQLiteHelper.DISABLESDB_NAME, null, null, null, null, null, null, null);
		
		
	    if (mCursor.moveToFirst()) {
	        do {  
	        	disabled_apps.add(mCursor.getString(mCursor.getColumnIndexOrThrow("package")));
	        } while (mCursor.moveToNext());
	    }
	    
	    //Close connection
	    if (mCursor != null && !mCursor.isClosed()) {
	        mCursor.close();
	    }
    	
    	return disabled_apps;
    }
    
    private List<ApplicationInfo> checkForLaunchIntent(List<ApplicationInfo> list) {
		ArrayList<ApplicationInfo> applist = new ArrayList<ApplicationInfo>();
		
		for (ApplicationInfo info : list) {
			try {
				if (null != packageManager.getLaunchIntentForPackage(info.packageName)) {
					applist.add(info);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return applist;
	}
    
    @Override
    public void onListItemClick(ListView l, View v, int pos, long id) {
    	super.onListItemClick(l, v, pos, id);
 
    	for (int i = 0; i < applist.size(); i++){
    		if (applist.get(i).loadLabel(packageManager) == ((TextView)v.findViewById(R.id.app_name)).getText().toString()){
	    		//Initialize SQLiteHelper
	    		dbHelper = new SQLiteHelper(getActivity());
	    		open();
	    		
	    		boolean disabled = false;
	    		
	    		for (int i1 = 0; i1 < disabled_apps.size(); i1++){
					if (disabled_apps.get(i1).contains(applist.get(i).packageName)){
						disabled = true;
						break;
					}
				}
	    		
	    		if(v.isActivated()){
    				if (disabled == true){
    					db.delete(SQLiteHelper.DISABLESDB_NAME, "package = ?", new String[] { applist.get(i).packageName });
    				} else {
    					ContentValues contentValues = new ContentValues();
    	    			contentValues.put("package", applist.get(i).packageName);

    	    			db.insert(SQLiteHelper.DISABLESDB_NAME, null, contentValues);
    				}
	    		} else {
	    			if (disabled != true){
    					db.delete(SQLiteHelper.DISABLESDB_NAME, "package = ?", new String[] { applist.get(i).packageName });
    				} else {
    					ContentValues contentValues = new ContentValues();
    	    			contentValues.put("package", applist.get(i).packageName);

    	    			db.insert(SQLiteHelper.DISABLESDB_NAME, null, contentValues);
    				}
	    		}
	    		
	    		close();
	    		break;
    		}
    	}
    }
    
	//Open the database connection
	public void open() throws SQLException {
		db = dbHelper.getWritableDatabase();
	}
	
	//Close the database connection
	public void close() {
		dbHelper.close();
	}
    
	private class LoadApplications extends AsyncTask<Void, Void, Void> {
		private ProgressDialog progress = null;

		@Override
		protected Void doInBackground(Void... params) {
			applist = checkForLaunchIntent(packageManager.getInstalledApplications(PackageManager.GET_META_DATA));
			disabled_apps = load_disabled_apps();
			Collections.sort(applist, new ApplicationInfo.DisplayNameComparator(packageManager));
			listadaptor = new AppsDrawerListAdapter(getActivity(), R.layout.drawer_apps_item, applist, disabled_apps);

			return null;
		}

		@Override
		protected void onCancelled() {
			super.onCancelled();
		}

		@Override
		protected void onPostExecute(Void result) {
			setListAdapter(listadaptor);
			progress.dismiss();
			super.onPostExecute(result);
		}

		@Override
		protected void onPreExecute() {
			progress = ProgressDialog.show(getActivity(), null, "Loading application info...");
			super.onPreExecute();
		}

		@Override
		protected void onProgressUpdate(Void... values) {
			super.onProgressUpdate(values);
		}
	}
}