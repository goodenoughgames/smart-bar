package com.example.smartbar;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLiteHelper extends SQLiteOpenHelper {
	private static final String DATABASE_NAME = "SmartBar";
	private static final int DATABASE_VERSION = 2;
	 
	public static final String MAINDB_NAME = "main";
	public static final String TIMINGDB_NAME = "timing";
	public static final String DISABLESDB_NAME = "disables";

	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL("CREATE TABLE IF NOT EXISTS  " + MAINDB_NAME + "(package VARCHAR, wifi INTEGER, bluetooth VARCHAR, latitude DOUBLE, longtitude DOUBLE, speed FLOAT, accuracy FLOAT,  rtime VARCHAR, rdate LONG);");
		database.execSQL("CREATE TABLE IF NOT EXISTS " + TIMINGDB_NAME + "(package VARCHAR, rtime INTEGER, percentage FLOAT, total INTEGER, UNIQUE (package, rtime) ON CONFLICT REPLACE);");
		database.execSQL("CREATE TABLE IF NOT EXISTS " + DISABLESDB_NAME + "(package VARCHAR, UNIQUE (package) ON CONFLICT REPLACE);");
	}
		
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		//Update from v.1
		if (oldVersion <= 1){
			db.execSQL("CREATE TABLE IF NOT EXISTS " + DISABLESDB_NAME + "(package VARCHAR, UNIQUE (package) ON CONFLICT REPLACE);");
		}
	}
} 