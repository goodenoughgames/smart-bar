package com.example.smartbar;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
public class SmartBar_Widget extends AppWidgetProvider {

@Override
  public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
	super.onUpdate(context, appWidgetManager, appWidgetIds);
	Intent intent;
	
	//Start controler
    intent = new Intent(context, controler.class);
    context.startService(intent);
    
    //Start Database Controler
    intent = new Intent(context, dbctrl.class);
    context.startService(intent);
    
    //Start manager
    intent = new Intent(context, manager.class);
    context.startService(intent);
 
   }
}