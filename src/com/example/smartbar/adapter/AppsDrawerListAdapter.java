package com.example.smartbar.adapter;

import java.util.List;
import com.example.smartbar.R;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class AppsDrawerListAdapter extends ArrayAdapter<ApplicationInfo> {
	private List<ApplicationInfo> appsList = null;
	private List<String> disabled_apps = null;
	private Context context;
	private PackageManager packageManager;

	public AppsDrawerListAdapter(Context context, int textViewResourceId, List<ApplicationInfo> appsList, List<String> disabled_apps) {
		super(context, textViewResourceId, appsList);
		this.context = context;
		this.appsList = appsList;
		this.disabled_apps = disabled_apps;
		packageManager = context.getPackageManager();
	}

	@Override
	public int getCount() {
		return ((null != appsList) ? appsList.size() : 0);
	}

	@Override
	public ApplicationInfo getItem(int position) {
		return ((null != appsList) ? appsList.get(position) : null);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (null == view) {
			LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = layoutInflater.inflate(R.layout.drawer_apps_item, null);
		}

		ApplicationInfo data = appsList.get(position);
		if (null != data) {
			TextView appName = (TextView) view.findViewById(R.id.app_name);
			ImageView iconview = (ImageView) view.findViewById(R.id.app_icon);
			
			appName.setText(data.loadLabel(packageManager));
			iconview.setImageDrawable(data.loadIcon(packageManager));
		
			view.setBackgroundResource(R.drawable.apps_list_selector);
			
			for (int i = 0; i < disabled_apps.size(); i++){
				if (disabled_apps.get(i).contains(data.packageName)){
						view.setBackgroundResource(R.drawable.apps_list_selector_disabled);
					break;
				}
			}
		}
		return view;
	}
};