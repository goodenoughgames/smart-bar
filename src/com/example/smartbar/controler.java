package com.example.smartbar;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Set;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.IBinder;
import android.os.PowerManager;
import android.text.TextUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

@SuppressLint("SimpleDateFormat") 
public class controler extends Service{	
	
	//SQLiteHelper variables
	private SQLiteHelper dbHelper;
	private SQLiteDatabase db;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		//Initialize SQLiteHelper
		dbHelper = new SQLiteHelper(this);
		
	    //Set alarm
		AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.setRepeating(AlarmManager.RTC, setTime(10).getTimeInMillis(), 10 * 1000, alarmIntentManager());
	}

	@Override
	public void onStart(Intent intent, int startId) {
		//Get app on top
	    Context context = this;
	    ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
	    List< ActivityManager.RunningTaskInfo > taskInfo = am.getRunningTasks(1); 
	    ComponentName componentInfo = taskInfo.get(0).topActivity;
	   
	    //Get Previous app from settings
	    SharedPreferences set = context.getSharedPreferences("settings", Context.MODE_PRIVATE);
	    String previous_app = set.getString("previous_app", " ");
	    
	    //Manage Screen OFF
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		if (!powerManager.isScreenOn()){
	    	//Save current app as previous one
	    	SharedPreferences.Editor editor = set.edit();
	    	editor.putString("previous_app", componentInfo.getPackageName());
	    	editor.commit();
	    	
	    	previous_app = componentInfo.getPackageName();
	    	stopSelf();
		}
	    
	    //If the app isnt the same as previous do next
	    if(!previous_app.contains(componentInfo.getPackageName())){
	    	
	    	//If app isnt launcher do next
	    	if(!componentInfo.getPackageName().contains(getLauncher())){
	    		
	    		//Set up PackageManager
			    PackageManager pm = context.getPackageManager();
				//Check if app is non-system app (if it has a launch intent)
			    if (pm.getLaunchIntentForPackage(componentInfo.getPackageName())!= null) {					    
				    
				    //Get WiFi Info
				    int wifi = getCurrentSsid(context);
				    
				    //Get Bluetooth Info
				    String bt = "null";
				    
				    //Does Bluetooth adapter exist?
				    BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
				    if(bluetoothAdapter != null){
					    Set<BluetoothDevice> pairedDevices = bluetoothAdapter.getBondedDevices();
					    if (pairedDevices.size() > 0) {
					    	bt = pairedDevices.iterator().next().getName();
					    }
				    }
				    
				    //Get location
				    LocationManager locationManager = (LocationManager)  getSystemService(Context.LOCATION_SERVICE);
				    Criteria criteria = new Criteria();
				    String provider = locationManager.getBestProvider(criteria, true);
				    Location location = locationManager.getLastKnownLocation(provider);
				    
				    Double latitude = (double) 0;
				    Double longtitude = (double) 0;
				    Float speed = (float) 0;
				    Float accuracy = (float) 0;
				    
				    if(location!=null){
				    	 //Save location variables
				         latitude= location.getLatitude();
				         longtitude= location.getLongitude();
				         speed = location.getSpeed();
				         accuracy = location.getAccuracy();
				    }
				    
				    //Get time
				    SimpleDateFormat Time = new SimpleDateFormat("HH:mm");
				    String newtime =  Time.format(new Date(System.currentTimeMillis()));
				    
				    //Get date
				    Calendar c = Calendar.getInstance();
				    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				    String date = df.format(c.getTime());

				    //Change string date to integer
				    SimpleDateFormat  format = new SimpleDateFormat("dd-MM-yyyy");  
				    try {  
				    	java.util.Date date_int = format.parse(date);
						
					    //Insert variables into DB
					    dbInsert(componentInfo.getPackageName(), wifi, bt, latitude, longtitude, speed, accuracy, newtime, date_int.getTime());
					} catch (ParseException e) {}
			    }
	    	}
	    	
    	//Save current app as previous one
    	SharedPreferences.Editor editor = set.edit();
    	editor.putString("previous_app", componentInfo.getPackageName());
    	editor.commit();
	    }
	}

	private String getLauncher(){
		//Get launcher application
		final Intent intent = new Intent(Intent.ACTION_MAIN); 
		intent.addCategory(Intent.CATEGORY_HOME); 
		final ResolveInfo res = getPackageManager().resolveActivity(intent, 0); 
		if (res.activityInfo == null) {
		    return "";
		} 
		
		if ("android".equals(res.activityInfo.packageName)) {
			return ".";   
		} else {
			return res.activityInfo.packageName;
		} 

	}
	
	private Calendar setTime(int seconds){
		//Calculate time
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(System.currentTimeMillis());
	    calendar.add(Calendar.SECOND, seconds);
	
	    return calendar;
	}
	
	private PendingIntent alarmIntentManager(){
		//Update intent
   		Intent intent = new Intent(this.getApplicationContext(), AlarmReciever.class);
   		intent.putExtra("run", 1);
   		PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 1, intent, 0);
   		
   		return alarmIntent;
	}
	
	private static int getCurrentSsid(Context context) {
		  //NetworkID of Wifi Connection
		  int ssid = -1;
		  ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		  NetworkInfo networkInfo = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
		  if (networkInfo.isConnected()) {
		      final WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		      final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
		      if (connectionInfo != null && !TextUtils.isEmpty(connectionInfo.getSSID())) {
		    	  ssid = connectionInfo.getNetworkId();
		      }
		  }
	return ssid;
	}
	
	private void dbInsert(String package_name, int wifi, String bluetooth, double latitude, double longtitude, float speed, float accuracy, String rtime, long rdate){
		
		open();
		//Insert values into DB
		ContentValues values = new ContentValues(); 
		values.put("package", package_name);
		values.put("wifi", wifi);
		values.put("bluetooth", bluetooth);
		values.put("latitude", latitude);
		values.put("longtitude", longtitude);
		values.put("speed", speed);
		values.put("accuracy", accuracy);
		values.put("rtime", rtime);
		values.put("rdate", rdate);
		db.insert("main", null, values); 
		close();		
		//Update visual after change of app
	    Intent intent = new Intent(this.getApplicationContext(), manager.class);
	    this.startService(intent);
	}

	
	@Override
	public void onDestroy() {
		//Cancel alarm
		AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.cancel(alarmIntentManager());
	}
	
	 //Open the database connection
	 public void open() throws SQLException {
		 db = dbHelper.getWritableDatabase();
	 }

	 //Close the database connection
	 public void close() {
		 dbHelper.close();
	 }

}
