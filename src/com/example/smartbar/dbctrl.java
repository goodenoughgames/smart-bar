package com.example.smartbar;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.os.IBinder;

public class dbctrl extends Service {

	//SQLiteHelper variables
	private SQLiteHelper dbHelper;
	private SQLiteDatabase db;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}
	
	@Override
	public void onCreate() {
		//Initialize SQLiteHelper
		dbHelper = new SQLiteHelper(this);
		
	    //Set alarm
		AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.setRepeating(AlarmManager.RTC, setTime(3600).getTimeInMillis(), 3600 * 1000, alarmIntentManager());
	}
	
	@Override
	public void onStart(Intent intent, int startId) {
		timing();
		
		//Update visual after the change of ranking
	    intent = new Intent(this.getApplicationContext(), manager.class);
	    this.startService(intent);
	}
	
	private Calendar setTime(int seconds){
		//Calculate time
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(System.currentTimeMillis());
	    calendar.add(Calendar.SECOND, seconds);
	
	    return calendar;
	}
	
	private PendingIntent alarmIntentManager(){
		//Update intent
   		Intent intent = new Intent(this.getApplicationContext(), AlarmReciever.class);
   		intent.putExtra("run", 3);
   		PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 3, intent, 0);
   		
   		return alarmIntent;
	}
	
	private void timing(){
		String[] select = new String[] {"package", "COUNT(package) as total"};
		
		//Declare counters and other variables
		int total = 0;
		String where_time = "00:";
		
		open();
		Cursor fCursor = db.query("main", select, null, null, "package", null, null);
		
	    if (fCursor.moveToFirst()) {
	        do {
	        	total = Integer.parseInt(fCursor.getString(fCursor.getColumnIndexOrThrow("total")));
	        	
	        	for(int i = 1; i <= 24; i++){
		    		String[] whereArgs = new String[] {fCursor.getString(fCursor.getColumnIndexOrThrow("package"))};
		        	
		    		if (i <= 9){
		    			where_time = "0" + String.valueOf(i) + ":";
		    		} else {
		    			where_time = String.valueOf(i) + ":";
		    		}
		    		
		    		//Get current hour
		    		open();
		        	Cursor sCursor = db.rawQuery("SELECT COUNT(package), rdate FROM main WHERE package = ? and rtime LIKE '%"+ where_time +"%'", whereArgs);
		            Calendar c = Calendar.getInstance();
		            int hour = c.get(Calendar.HOUR_OF_DAY);
		        	
		            //Get current date
				    c = Calendar.getInstance();
				    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
				    String date = df.format(c.getTime());
				    
				    try {  
				    	java.util.Date date_int = df.parse(date);
				    	
				    	if (sCursor.moveToFirst()) {
			    	        do {
			    	        	if(date_int.getTime() != sCursor.getLong(sCursor.getColumnIndexOrThrow("rdate")) && i != hour){
				    	        	int count = sCursor.getInt(sCursor.getColumnIndexOrThrow("COUNT(package)"));
				    	        	dbtInsert(fCursor.getString(fCursor.getColumnIndexOrThrow("package")), i, count / total, count);
			    	        	}
			    	        } while (sCursor.moveToNext());
			    	    }
					} catch (ParseException e) {}
		    	    
		    	    //Close connection
		    	    if (sCursor != null && !sCursor.isClosed()) {
		    	        sCursor.close();
		    	    }
	        	}

	        } while (fCursor.moveToNext());
	    }
	    
	    //Close connection
	    if (fCursor != null && !fCursor.isClosed()) {
	        fCursor.close();
	    }
	    
	    close();
	}
	
	private void dbtInsert(String package_name, Integer rtime, float percentage, int total){
		
		//Insert values into DB
		open();
		ContentValues values = new ContentValues(); 
		values.put("package", package_name);
		values.put("rtime", rtime);
		values.put("percentage", percentage);
		values.put("total", total);
		db.insert("timing", null, values); 
		close();
	}
	
	 //Open the database connection
	 public void open() throws SQLException {
		 db = dbHelper.getWritableDatabase();
	 }

	 //Close the database connection
	 public void close() {
		 dbHelper.close();
	 }
	
}
