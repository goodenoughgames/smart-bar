package com.example.smartbar;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.RemoteViews;


@SuppressLint("SimpleDateFormat") 
public class manager extends Service {
	//Multipliers
	public static final int CONSTANT_BASIC_MULTIPLIER = 1;
	public static final int CONSTANT_TIME_MULTIPLIER = 5;
	
	//SQLiteHelper variables
	private SQLiteHelper dbHelper;
	private SQLiteDatabase db;
	
	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public void onCreate() {
		//Initialize SQLiteHelper
		dbHelper = new SQLiteHelper(this);
		
	    //Set alarm
		AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.setRepeating(AlarmManager.RTC, setTime(600).getTimeInMillis(), 600 * 1000, alarmIntentManager());
	}
	
	@Override
	public void onStart(Intent intent, int startId) {		
		//Manage Screen OFF
		PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
		if (!powerManager.isScreenOn()){  
	    	stopSelf();
		}
		
	    //Open settings
	    SharedPreferences set = this.getSharedPreferences("settings", Context.MODE_PRIVATE);
		
	    //Select from the database
	    open();
		Cursor mCursor = db.query("main", null, null, null, null, null, null, null);
		
		//Create HashMap
		Map<String, Integer> data = new HashMap<String, Integer>();
		Map<String, Integer> data_sorted = new HashMap<String, Integer>();
		
	    if (mCursor.moveToFirst()) {
	        do {  
	        	
	        	//Apps used in the last 7 days - 1 point per use
				long diffInMis = Calendar.getInstance().getTimeInMillis() - Long.parseLong(mCursor.getString(mCursor.getColumnIndexOrThrow("rdate")));
				long diffInDays = TimeUnit.MILLISECONDS.toDays(diffInMis);
				
				if (diffInDays <= 7){
					
					//If Package already ranked add CONSTANT_BASIC_MULTIPLIER
					if(data.get(mCursor.getString(mCursor.getColumnIndexOrThrow("package"))) != null){
						data.put(mCursor.getString(mCursor.getColumnIndexOrThrow("package")) , data.get(mCursor.getString(mCursor.getColumnIndexOrThrow("package"))) + CONSTANT_BASIC_MULTIPLIER);
					} else {
						data.put(mCursor.getString(mCursor.getColumnIndexOrThrow("package")) , CONSTANT_BASIC_MULTIPLIER);
					}
				}
	        } while (mCursor.moveToNext());
	    }
	    
	    //Close connection
	    if (mCursor != null && !mCursor.isClosed()) {
	        mCursor.close();
	    }
	    
		//Open and select from database
        Calendar c = Calendar.getInstance();
        String[] where_args = {String.valueOf(c.get(Calendar.HOUR_OF_DAY))};
        
		mCursor = db.rawQuery("SELECT * FROM " + SQLiteHelper.TIMINGDB_NAME +" WHERE rtime = ?", where_args);
		
	    if (mCursor.moveToFirst()) {
	        do {
					int score = 0;
					
					if (data.get(mCursor.getString(mCursor.getColumnIndexOrThrow("package"))) != null){
						score = data.get(mCursor.getString(mCursor.getColumnIndexOrThrow("package")));
						score += Math.round(mCursor.getFloat(mCursor.getColumnIndexOrThrow("percentage")) * mCursor.getInt((mCursor.getColumnIndexOrThrow("percentage"))) * CONSTANT_TIME_MULTIPLIER);
					}
					
					data.put(mCursor.getString(mCursor.getColumnIndexOrThrow("package")) , score);
					
	        } while (mCursor.moveToNext());
	    }
	    
	    //Close connection
	    if (mCursor != null && !mCursor.isClosed()) {
	        mCursor.close();
	    }
	    
	    //Create connection with Disables database
	    mCursor = db.rawQuery("SELECT * FROM " + SQLiteHelper.DISABLESDB_NAME, null);
	    List<String> disabled_apps = new ArrayList<String>();
	    
	    //Load disabled applications
	    if (mCursor.moveToFirst()) {
	        do {					
				disabled_apps.add(mCursor.getString(mCursor.getColumnIndexOrThrow("package")));				
	        } while (mCursor.moveToNext());
	    }
	    
	    //Close connection
	    if (mCursor != null && !mCursor.isClosed()) {
	        mCursor.close();
	    }
	    
	    //Sort data by score
	    data_sorted =sortByComparator(data);
	    
	    //Set up layout update
	    AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(this);
	    RemoteViews remoteViews = new RemoteViews(this.getPackageName(), R.layout.main);
	    ComponentName thisWidget = new ComponentName(this, SmartBar_Widget.class);
	    
	    //Set up counter
	    int i = 0;
	    
	    for (Map.Entry<String,Integer> entry : data_sorted.entrySet()) {	  
	    	i += 1; 
	    	
    		//Update Imageview
		    PackageManager pm = this.getPackageManager();
			Drawable icon;
			    
			if (disabled(entry.getKey(), disabled_apps)){		
		    	try {
					icon = pm.getApplicationIcon(entry.getKey());
					
					//Update the imageviews based on rank
					if(i==1){		
						remoteViews.setImageViewBitmap(R.id.update1, ((BitmapDrawable)icon).getBitmap());
					    appWidgetManager.updateAppWidget(thisWidget, remoteViews);
					} else if(i==2){
						remoteViews.setImageViewBitmap(R.id.update2, ((BitmapDrawable)icon).getBitmap());
					    appWidgetManager.updateAppWidget(thisWidget, remoteViews);
					} else if(i==3){	
						remoteViews.setImageViewBitmap(R.id.update3, ((BitmapDrawable)icon).getBitmap());
					    appWidgetManager.updateAppWidget(thisWidget, remoteViews);
					} else if(i==4){
						remoteViews.setImageViewBitmap(R.id.update4, ((BitmapDrawable)icon).getBitmap());
					    appWidgetManager.updateAppWidget(thisWidget, remoteViews);
					} else {
						break;
					}
		
			    	//Save current app on a button
			    	SharedPreferences.Editor editor = set.edit();
			    	editor.putString("update" + String.valueOf(i), entry.getKey());
			    	editor.commit();
				} catch (NameNotFoundException e) {}
			} else {
				i--;
			}
	    }
	    
	    
	    while (i < 4){
	    	i += 1; 
	    	
	    	//Save com.example.smartbar on a button so that something starts when first run
	    	SharedPreferences.Editor editor = set.edit();
	    	editor.putString("update" + String.valueOf(i), "com.example.smartbar");
	    	editor.commit();
	    	
	    }
	    
	    //Get ids of running widgets
		AppWidgetManager widgetManager = AppWidgetManager.getInstance(getBaseContext());
		ComponentName widgetComponent = new ComponentName(getBaseContext(), SmartBar_Widget.class);
		int[] widgetIds = widgetManager.getAppWidgetIds(widgetComponent);
		
		for (int widgetId : widgetIds) {
			PendingIntent pendingIntent;
			
			// Register an onClickListener
			intent = new Intent(getBaseContext(), AlarmReciever.class);
			
			intent.setAction(AlarmReciever.UPDATE1);
			pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 11, intent, PendingIntent.FLAG_UPDATE_CURRENT);    
			remoteViews.setOnClickPendingIntent(R.id.update1, pendingIntent);
			
			intent.setAction(AlarmReciever.UPDATE2);
			pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 12, intent, PendingIntent.FLAG_UPDATE_CURRENT);    
			remoteViews.setOnClickPendingIntent(R.id.update2, pendingIntent);

			intent.setAction(AlarmReciever.UPDATE3);
			pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 13, intent, PendingIntent.FLAG_UPDATE_CURRENT);    
			remoteViews.setOnClickPendingIntent(R.id.update3, pendingIntent);
			
			intent.setAction(AlarmReciever.UPDATE4);
			pendingIntent = PendingIntent.getBroadcast(getBaseContext(), 14, intent, PendingIntent.FLAG_UPDATE_CURRENT);    
			remoteViews.setOnClickPendingIntent(R.id.update4, pendingIntent);
			
			appWidgetManager.updateAppWidget(widgetId, remoteViews);
	    }
		
		close();
	}
	
	@Override
	public void onDestroy() {
		//Cancel alarm
		AlarmManager alarmMgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
		alarmMgr.cancel(alarmIntentManager());
	}
	
	private Calendar setTime(int seconds){
		//Calculate time
	    Calendar calendar = Calendar.getInstance();
	    calendar.setTimeInMillis(System.currentTimeMillis());
	    calendar.add(Calendar.SECOND, seconds);
	
	    return calendar;
	}
	
	private PendingIntent alarmIntentManager(){
		//Update intent
   		Intent intent = new Intent(this.getApplicationContext(), AlarmReciever.class);
   		intent.putExtra("run", 2);
   		PendingIntent alarmIntent = PendingIntent.getBroadcast(this, 2, intent, 0);
   		
   		return alarmIntent;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static Map<String, Integer> sortByComparator(Map unsortMap) {

		List list = new LinkedList(unsortMap.entrySet());
 
		// sort list based on comparator
		Collections.sort(list, new Comparator() {
			public int compare(Object o1, Object o2) {
				return ((Comparable) ((Map.Entry) (o2)).getValue()).compareTo(((Map.Entry) (o1)).getValue());
			}
		});
 
		// put sorted list into map again
		//LinkedHashMap make sure order in which keys were inserted
		Map sortedMap = new LinkedHashMap();
		for (Iterator it = list.iterator(); it.hasNext();) {
			Map.Entry entry = (Map.Entry) it.next();
			sortedMap.put(entry.getKey(), entry.getValue());
		}
		return sortedMap;
	}
	
	 //Open the database connection
	 public void open() throws SQLException {
		 db = dbHelper.getWritableDatabase();
	 }

	 //Close the database connection
	 public void close() {
		 dbHelper.close();
	 }
	 
	 //Dont add disabled apps
	 public boolean disabled(String package_name, List<String> disabled_apps){
		 for (int i = 0; i < disabled_apps.size(); i++){
			 if (package_name.contains(disabled_apps.get(i))){
				 return false;
			 }	 
		 }
		 
		 if (package_name.contains("smartbar")){
			 return false;
		 }
		 
		 return true;
	 }
}
